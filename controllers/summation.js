/**
 * GET /
 */
exports.summation = function(req, res) {
  res.render('summation', {
    title: 'Cube Summation'
  });
};

// **
// * POST /
// */
exports.calculate = function(req, res) {
  var input = req.body.ta_input;
  var result = doOperations(input);
  res.json({ "result" : result });
};


var getOperation = function(line){
  // Only allow UPDATE and QUERY
  var allowed = ['UPDATE','QUERY'];
  var arr_op = line.split(" ");
  var operation = arr_op[0];
  if (allowed.indexOf(operation) < 0){
    return operation + ' Is not a valid operation, not allowed!';
  }
  return arr_op;
};

var createMatrix = function(n){
  // TODO: It can be better
  var n_matrix = [];
  for (_i = 0; _i < n; _i++){
    n_matrix[_i] = [];
    for (_j = 0; _j < n; _j++){
      n_matrix[_i][_j] = [];
      for (_k = 0; _k < n; _k++){
        n_matrix[_i][_j][_k] = 0;
      }
    }
  }
  return n_matrix;
}

var updateMatrix = function(matrix,x,y,z,w){
  //TODO: Add validations
  var _matrix = matrix;
  _matrix[parseInt(x)-1][parseInt(y)-1][parseInt(z)-1] = parseInt(w);
  return parseInt(w);
};

var doQuery = function(matrix,x1,y1,z1,x2,y2,z2){
  var _matrix = []
  _matrix = matrix;
  var res = 0;
  for (qi = x1; qi <= x2; qi++){
    for (qj = y1; qj <= y2; qj++){
      for (qk = z1; qk <= z2; qk++){
        if (parseInt(_matrix[parseInt(qi)-1][parseInt(qj)-1][parseInt(qk)-1]) > 0 ){
          res += parseInt(_matrix[parseInt(qi)-1][parseInt(qj)-1][parseInt(qk)-1]);
        }
      }
    }
  }
  return res;
};

var executeOperation = function(op,matrix){
  var opRes = '';
  if(op[0] === 'QUERY'){
    opRes = doQuery(matrix,op[1],op[2],op[3],op[4],op[5],op[6]);
    // console.log(res);
    return opRes;
  }else if (op[0] === 'UPDATE'){
    updateMatrix(matrix,op[1],op[2],op[3],op[4])
  }
};

// Receives the array with the input_line and
// parseit in order to execute the test-cases
var doOperations = function(list){
  var input_line = list.split('\n');
  var t = input_line[0];
  var n = 0;
  var m = 0;
  var _m = m;
  var test_case = 1;
  var m_info = '';
  var operation = '';
  var result = '';
  var my_matrix = [];
  var output_result = '';
  for (i = 1; i <= t; i++) {
    test_case = test_case + parseInt(m);
    m_info = input_line[test_case].split(" ");
    n = m_info[0];
    m = m_info[1];
    _m = m;
    //create the matrix
    my_matrix = createMatrix(n);
    for (j = 1; j <= _m; j++){
      // Gets an array with the operation and its parameters after validations
      operation = getOperation(input_line[test_case+j]);
      // console.log("operation : "+operation);
      result = executeOperation(operation,my_matrix);
      if (operation.indexOf('QUERY') > -1){
        output_result += result + '\n';
      }
    }
    my_matrix = [];
    res = '';
    m++;
  }
  // console.log(output_result);
  return output_result;
};
