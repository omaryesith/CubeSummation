$(function() {
  var drawTestCases = function(){
    $("#t").keyup();
    $(".operations").keyup();
  };

  var getInput = function(){
    var t = $("#t").val();
    var input = t+'\n';
    var qop = 0;

    for (i = 1; i<=t; i++){
      qop = $("#m"+i).val();
      input += $("#n"+i).val() + ' ' + qop + '\n';
      for (j = 1; j<=qop; j++){
        input += $("#tc"+i+"_op"+j).val() + '\n';
      }
    }

    $("#ta_input").html(input);
  };

  $("#t").bind('keyup mouseup', function () {
    var t = parseInt(this.value);
    $("#test-cases").html('');
    for (i = 1; i<=t; i++){
      test_case = `<div class="row-fluid">
                    <div class="span12">
                      <div class="row-fluid">
                        <div class="span12">
                          <p><strong>Test Case #${i}</strong></p>
                        </div>
                      </div>
                      <div class="row well">
                        <div class="row-fluid">
                          <div class="span6">
                            <label class="col-sm-2" for="n${i}">N</label>
                            <div class="col-sm-4">
                              <input type="number" class="input-mini" required="" id="n${i}" name="n${i}" value="1" max="100" min="1">
                            </div>
                          </div>
                          <div class="span6">
                            <label class="col-sm-2" for="m${i}">M</label>
                            <div class="col-sm-4">
                              <input type="number" class="input-mini operations" required="" id="m${i}" name="m${i}" value="1" max="100" min="1" data-tc="${i}">
                            </div>
                          </div>
                        </div>
                        <div class="row-fluid" id="operations${i}"></div>
                      </div>
                    </div>
                  </div>`;
      $("#test-cases").append(test_case);
    }
    bindOperations();
  });

  var bindOperations = function(){
    $(".operations").bind('keyup mouseup', function () {
      var cantOp = parseInt(this.value);
      var tc = $("#"+this.id).data('tc');
      $("#operations"+$("#"+this.id).data('tc')).html('');
      for (op = 1; op<=cantOp; op++){
        operation = `<div class="span4">
                        <div class="col-sm-4">
                          ${op}
                          <input type="text" class="op input-mini" required="true" id="tc${tc}_op${op}" name="tc${tc}_op${op}" data-op="tc${tc}_op${op}">
                        </div>
                      </div>`;
        $("#operations"+$("#"+this.id).data('tc')).append(operation);
      }
      getInput();
    });
    getInput();

  };

  $(document).on('keyup', '.op', function() {
    getInput();
  });

  $("#btn_calculate").click( function(){
    $.ajax({
      url: '/calculate',
      dataType: 'json',
      type: 'POST',
      data: { 'ta_input' : $("#ta_input").val() },
      success: function(data, status){
        $("#ta_output").html(data.result);
      }
    });
  });

  $( document ).ready(function() {
    drawTestCases();
  });
});
