var request = require('supertest');
var server = require('../server');
var should = require('should');

describe('GET /', function() {
  it('should render ok', function(done) {
    request(server)
      .get('/')
      .expect(200, done);
  });
});

describe('GET /summation', function() {
  it('should render ok', function(done) {
    request(server)
      .get('/summation')
      .expect(200, done);
  });
});

describe('POST /calculate', function() {
  it('should render ok and calculate the sum correctly', function(done) {
    request(server)
      .post('/calculate')
      .send({ 'ta_input' : '2\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\n2 4\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 2 2 2 2 2 2'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) done(err);
        res.body.should.have.property('result', '4\n4\n27\n0\n1\n1\n');
        done();
      })
  });
});
